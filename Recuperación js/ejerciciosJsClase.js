/*function comprobarForm() {
    var lstCampos = document.getElementsByClassName("form-control campo")
    var array = [];
    for (var i = 0; i < lstCampos.length; i++) {
        array[i] = lstCampos[i].value;


        if (array[i].value === null) {  //Preguntar mañana como hacer que compruebe que todos los campos estén rellenos
            alert("Debe rellenar todos los campos");
        } else {
            var correo = document.getElementById('email');
            var correoRep = document.getElementById('repEmail');
    
            if (correo.value != correoRep.value) {
                alert('Los correos no coinciden. Por favor revíselos');
            }else{
                var contra = document.getElementById('contra');
                var contraRep = document.getElementById('repContra');
            
                if(contra.value != contraRep.value){ 
                    alert('Las contraseñas no coindicen. Revíselas');
                }else{
                    alert('Todo correcto');
                }
            }
        }

    }
}*/

//a) El nombre y los apellidos deben tener únicamente letras
//TODO la primera letra de cada palabra debe estar en mayúscula

var soloLetras = new RegExp('^[A-Za-zÑñÁÉÍÓÚáéíóú ]+$');
var codPostal = document.getElementById("codPostal");

function comprobarNom() {
    var nombre = document.getElementById("nombre");
    var mensajeNom = document.getElementById("mensajeNom");

    if (soloLetras.test(nombre.value)) {
        nombre.style.borderColor = "green";
        mensajeNom.innerText = "";
    } else {
        nombre.style.borderColor = "red";
        mensajeNom.innerText = "El nombre introducido no es válido";
        mensajeNom.style.color = "red";
    }
}

function comprobarApellido() {
    var apellido = document.getElementById("lastName");
    var mensajeLastName = document.getElementById("mensajeLastName");

    if (soloLetras.test(apellido.value)) {
        apellido.style.borderColor = "green";
        mensajeLastName.innerText = "";
    } else {
        apellido.style.borderColor = "red";
        mensajeLastName.innerText = "El apellido introducido no es válido";
        mensajeLastName.style.color = "red";
    }
}


//b)
function comprobarEmail() {
    var emails = document.getElementsByClassName("form-control email");
    var mensajeEmail = document.getElementById("mensajeEmail");

    if (emails[0].value != emails[1].value || emails[0].value === "") {
        for (var i = 0; i < emails.length; i++) {
            emails[i].style.borderColor = "red";
        }
        mensajeEmail.innerText = "Los correos no coinciden";
        mensajeEmail.style.color = "red";
    } else {
        for (var i = 0; i < emails.length; i++) {
            emails[i].style.borderColor = "green";
            mensajeEmail.innerText = "";
        }
    }

}

//d) El código postal debe ser un número de 5 cifras
function comprobarCodPostal() {
    var solosNumeros = new RegExp('^[0-9]+$');
    var mensajePostal = document.getElementById("mensajePostal");

    if (codPostal.value.length != 5 || !solosNumeros.test(codPostal.value)) {
        codPostal.style.borderColor = "red";
        mensajePostal.innerText = "El código postal no existe";
        mensajePostal.style.color = "red";

    } else {
        codPostal.style.borderColor = "green";
        mensajePostal.innerText = "";
    }
}


//e) La provincia debe coincidir con el código postal
//TODO borde campo
function comprobarProvincia() {
    var provincia = document.getElementById("provinciaAndalucia");
    var mensajeProvincia = document.getElementById("mensajeProvincia");
    var codigoTrunc = Math.trunc(codPostal.value / 1000);
    var correcto = false;

    switch (provincia.value) {
        case 'Almería':
            if (codigoTrunc == 4) {
                correcto = true;
            }
            break;
        case 'Cádiz':
            if (codigoTrunc == 11) {
                correcto = true;
            }
            break;
        case 'Córdoba':
            if (codigoTrunc == 14) {
                correcto = true;
            }
            break;
        case 'Granada':
            if (codigoTrunc == 18) {
                correcto = true;
            }
            break;
        case 'Huelva':
            if (codigoTrunc == 21) {
                correcto = true;
            }
            break;
        case 'Jaén':
            if (codigoTrunc == 23) {
                correcto = true;
            }
            break;
        case 'Sevilla':
            if (codigoTrunc == 41) {
                correcto = true;
            }
            break;
        default:
            correcto = true;
            break;
    }

   if(correcto && provincia.value != ""){
       provincia.style.borderColor = "green";
       mensajeProvincia.innerText = "";
   }else{
       provincia.style.borderColor = "red";
       mensajeProvincia.innerText = "La provincia no corresponde con el código postal";
       mensajeProvincia.style.color = "red";
   }

}


//f) La contraseñas deben coincidir
function comprobarContra() {
    contrasenas = document.getElementsByClassName("form-control contra");
    var mensajeContra = document.getElementById("mensajeContra");

    if (contrasenas[0].value != contrasenas[1].value || contrasenas[0].value === "") {
        for (var i = 0; i < contrasenas.length; i++) {
            contrasenas[i].style.borderColor = "red";
        }
        mensajeContra.innerText = "Los emails son distintos";
        mensajeContra.style.color = "red";
    } else {
        for (var i = 0; i < contrasenas.length; i++) {
            contrasenas[i].style.borderColor = "green";
            mensajeContra.innerText = "";
        }
    }
}
