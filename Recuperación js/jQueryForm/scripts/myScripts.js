var clicked = false;
var count = 0;
var strength = 0;
$(document).ready(function () {

    //Campo nombre
    $("#name").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            if (onlyLetters($(this).val())) {
                $(this).css("border-color", "#7CFC00");
                count++;
            }
            else {
                $(this).css("border-color", "#DC143C");
                count--;
            }
        }
        isReady();
    });

    //Campo apellido
    $("#surname").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            if (onlyLetters($(this).val())) {
                $(this).css("border-color", "#7CFC00");
                count++;
            }else {
                $(this).css("border-color", "#DC143C");
                count--; 
            }
        }
        isReady();
    });

    //Campo email
    $("#email").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            if (validateEmail($(this).val())) {
                $(this).css("border-color", "#7CFC00");
                count++;
            } else {
                $(this).css("border-color", "#DC143C");
                count--;
            }
        }
        isReady();
    });
    //Campo confirmacion email
    $("#email2").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            if ($(this).val() == $("#email").val()) {
                $(this).css("border-color", "#7CFC00");
                count++;
            } else {
                $(this).css("border-color", "#DC143C");
                count--;
            }
        }
        isReady();
    });

    //Campo contraseña
    $("#passwordOne").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            $(this).css("border-color", "#7CFC00");
            count++;
        }
        checkPassword()
        isReady();
    });

    //Campo confirmacion contraseña
    $("#passwordTwo").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            if ($(this).val() == $("#passwordOne").val()) {
                $(this).css("border-color", "#7CFC00");
                count++;
            } else {
                $(this).css("border-color", "#DC143C");
                count--;
            }
        }
        isReady();
    });

    //Campo dirección
    $("#direction").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            $(this).css("border-color", "#7CFC00");
            count++;
        }
        isReady();
    });
    //Campo codigo postal
    $("#postalcode").blur(function () {
        if ($(this).val() == '') {
            $(this).css("border-color", "#DC143C");
            count--;
        } else {
            if (validatePostalCode($(this).val())) {
                checkProvince($(this).val());
                $(this).css("border-color", "#7CFC00");
                $("#province").css("border-color", "#7CFC00");
                count++;
            } else {
                $(this).css("border-color", "#DC143C");
                count--;
            }
        }
        isReady();
    });
    //Campo provincia
    $("#province").blur(function () {
        if ($(this).val() == '') {
            count--;
            $(this).css("border-color", "#DC143C");
        } else {
            if (onlyLetters($(this).val())){
            $(this).css("border-color", "#7CFC00");
            count++;
            }else {
            $(this).css("border-color", "#DC143C");
            count--;
            }
        }
        isReady();
    });

    //Boton contraseña con click
    $("#generatePass").click(function () {
        var pass = randomPassword();
        $("#passwordOne").attr("value", '');
        $("#passwordTwo").attr("value", '');
        $("#passwordOne").attr("value", pass);
        $("#passwordTwo").attr("value", pass);
        if ($("#passwordOne").val() == '') {
            $("#passwordOne").css("border-color", "#DC143C");
        } else {
            if (validatePassword($("#passwordOne").val())) {
                $("#passwordOne").css("border-color", "#7CFC00");
            } else {
                $("#passwordOne").css("border-color", "#DC143C");
            }
        }
        if ($("#passwordTwo").val() == '') {
            $("#passwordTwo").css("border-color", "#DC143C");
        } else {
            if ($("#passwordTwo").val() == $("#passwordOne").val()) {
                $("#passwordTwo").css("border-color", "#7CFC00");
            } else {
                $("#passwordTwo").css("border-color", "#DC143C");
            }
        }
        count++;
        count++;
        checkPassword();
        isReady();
    });

    //Boton enseñar contraseña
    $("#showPass").on({
        mouseenter: function () {
            $("#passwordOne").attr("type", "text");
            $("#passwordTwo").attr("type", "text");
            $("#lock").text("👁");
        },
        mouseleave: function () {
            if (clicked == false) {
                $("#passwordOne").attr("type", "password");
                $("#passwordTwo").attr("type", "password");
                $("#lock").text("🔒");
            }
        },
        click: function () {
            /* var tipo = $("#passwordOne").attr("type");
             var tipoNuevo = (tipo==="text")?"password":"text";
             $("#passwordOne").attr("type", tipoNuevo);
             $("#passwordTwo").attr("type", tipoNuevo);
             
             $("#passwordOne").attr("type")==="text"?$("#passwordOne").attr("type", "password"):$("#passwordOne").attr("type", "text");
             */
            if (checkType()) {
                $("#passwordOne").attr("type", "text");
                $("#passwordTwo").attr("type", "text");
                $("#lock").text("👁");
                clicked = true;
            } else {
                $("#passwordOne").attr("type", "password");
                $("#passwordTwo").attr("type", "password");
                clicked = false;
            }
        }
    });

});

//Funciones
//Comprobar email con pattern
function validateEmail(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}

//Generador contraseñas
function randomPassword() {
    var length = 12;
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}


/*Comprobar contraseña con pattern (Mínimo 8 caracteres y max 15. Al menos una letra mayuscula.
    Al menos una letra minuscula. Al menos un digito. No espacios en blanco. Al menos un caracter especial.)
function validatePassword(password) {
    var pattern = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/;
    return pattern.test(password);
}
*/

function checkPassword() {
    $('#strongVerification').removeClass();
    $('#strongVerification').text('');

    var password = $("#passwordOne").val();
    //Demasiado corto
    if (password.length < 6) {
        $('#strongVerification').addClass("col-4", "vlows").text("Muy débil");
    }
    if (password.length > 6) {
        strength += 1;
    }
    //Si contiene mayúsculas y minúsculas
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/)) {
        strength += 1;
    }
    //Si tiene letras y números
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/)) {
        strength += 1;
    }
    //Si tiene 1 carácter especial
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)) {
        strength += 1;
    }


    switch (strength) {
        case 1:
            $('#strongVerification').addClass("form-text col-4 vlows").text("Muy débil");
            break;
        case 2:
            $('#strongVerification').addClass("form-text col-5 lows").text("Débil");
            break;
        case 3:
            $('#strongVerification').addClass("form-text col-6 mediums").text("Media");
            break;
        case 4:
            $('#strongVerification').addClass("form-text col-10 strongs").text("Alta");
            break;
        default:
            break;
    }
}

//Comprobar si esta clickado
function checkType() {
    if (clicked == false) {
        if ($("#passwordOne").attr("type") == 'text') {
            return true;
        }
    }
    if (clicked == true) {
        return false;
    }
}


//Comprobar código postal
function validatePostalCode(postalCode) {
    var pattern = /^(?:0[1-9]|[1-4]\d|5[0-2])\d{3}$/;
    return pattern.test(postalCode);
}

//Para autoescribir la provincia por código postal
function checkProvince(postalCode) {
    var digits = Math.trunc(postalCode / 1000);
    var province;

    switch (digits) {
        //Almería
        case 4:
            province = 'Almería';
            break;
        //Cádiz
        case 11:
            province = 'Cádiz';
            break;
        //Córdoba
        case 14:
            province = 'Córdoba';
            break;
        //Granada
        case 18:
            province = 'Granada';
            break;
        //Huelva
        case 21:
            province = 'Huelva';
            break;
        //Jaén
        case 23:
            province = 'Jaén';
            break;
        //Sevilla
        case 41:
            province = 'Sevilla';
            break;
        default:
            break;
    }
    $("#province").attr("value", province);
}

//Para comprobar que la provincia coincide con el código postal
function checkProvincePostal(province) {
    var postalcode = Math.trunc($("#postalcode") / 1000);
    var isTrue;

    switch (province) {
        case 'Almería':
            if (postalcode == 4) {
                isTrue = true;
            }
            break;
        case 'Cádiz':
            if (postalcode == 11) {
                isTrue = true;
            }
            break;
        case 'Córdoba':
            if (postalcode == 14) {
                isTrue = true;
            }
            break;
        case 'Granada':
            if (postalcode == 18) {
                isTrue = true;
            }
            break;
        case 'Huelva':
            if (postalcode == 21) {
                isTrue = true;
            }
            break;
        case 'Jaén':
            if (postalcode == 23) {
                isTrue = true;
            }
            break;
        case 'Sevilla':
            if (postalcode == 41) {
                isTrue = true;
            }
            break;
        default:
            isTrue = true;
            break;
    }
    return isTrue;
}
//Funcion que comprueba si el formulario esta listo para el envio
function isReady() {

    //Si el valor es menor que 0, lo setea a 0.
    if (count < 0) {
        count = 0;
    }
    //Si el valor es mayor que 9, lo setea 9.
    if (count > 9) {
        count = 9;
    }

    if (count == 9) {
        $("#submit").removeClass("deactivate");
        $("#submit").prop("disabled", false);
    } else {
        $("#submit").addClass("deactivate");
        $("#submit").prop("disabled", true);
    }
}

//Comprobar solo letras
function onlyLetters(variable) {
    return variable.match(/^([a-zA-Z]+\s)*[a-zA-Z]+$/);
}