var valores = [true, 5, false, "hola", "adios", 2];

/*Apartado a*/
var mayor = (valores[3] > valores[4])? valores[3]:valores[4];   /*Será mayor si está después por orden alfabético. Para hacerlo por tamaño habría que hacer .legth*/

/*Apartado b*/
var resulTrue = (valores[0]||valores[2]==false)?true:false;   /*En caso de que valores[0] o valores [2] valgan "false" entonces devuelve true, sino devuelve false*/
/*var resulTrue = valores[0] || valores[2]*/
var resulFalse = (valores[0]!=valores[2])?false:true;  /*En caso de que valores[0] y valores[2] valgan distinto devuelve false*/
/*var resulFalse = !valores[0] && valores[2];*/


/*Apartado c*/
var sumar = valores[1] + valores[5];
var restar = valores[1] - valores[5];
var multiplicar = valores[1] * valores[5];;
var dividir = valores[1] / valores[5];;
var resto = valores[1] % valores[5];

alert("a) Es mayor (está detrás en el alfabeto) es: "+mayor);
alert("b.1)"+resulTrue); 
alert("b.2"+resulFalse);
alert("c.1) El resultado de la suma es "+sumar);
alert("c.2) El resultado de la resta es "+restar);
alert("c.3) El resultado de multiplicar es "+multiplicar);
alert("c.4) El resultado de dividir es "+dividir);
alert("c.5) El resto de la divión es "+resto);